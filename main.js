console.log('page load - entered main.js for Daily13');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var zip = document.getElementById("name-text").value;
    console.log('Zip code entered:  ' + zip);
    makeNetworkCallToAgeApi(zip);

} // end of get form info

function makeNetworkCallToAgeApi(zip){
    console.log('entered make nw call' + zip);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.zippopotam.us/us/" + zip;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateZipWithResponse(zip, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateZipWithResponse(zip, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("response-line1");

  if (response_json == null) {
		label1.innerHTML = 'Apologies, we could not find your zip code.'
	} else {
		var place = response_json;
		label1.innerHTML = zip + ': Zip code correlates to --> ' + response_json['places'][0]['place name'];
		var place = response_json['places'][0]['place name'];
		makeNetworkCallToNumbers(place);
	}
} // end of updateZipWithResponse


function makeNetworkCallToNumbers(place){
    console.log('entered make nw call' + place);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.genderize.io?name=" + place;
    xhr.open("GET", url, true) // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updatePlaceWithResponse(place, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updatePlaceWithResponse(place, response_text){
    // update a label 
    var response_json = JSON.parse(response_text)
    var label2 = document.getElementById("response-line2");
    label2.innerHTML = 'Your city name means you are: ' + response_json['gender']
    //label2.innerHTML = response_json['gender'];

    // dynamically adding label
    label_item = document.createElement("label"); // "label" is a classname
    label_item.setAttribute("id", "dynamic-label" ); // setAttribute(property_name, value) so here id is property name of button object

    
    //label_item.appendChild(item_text); // adding something to button with appendChild()

    // option 1: directly add to document
    // adding label to document
    //document.body.appendChild(label_item);

    // option 2:
    // adding label as sibling to paragraphs
    var response_div = document.getElementById("response-div");
    response_div.appendChild(label_item);

} // end of updateTriviaWithResponse
